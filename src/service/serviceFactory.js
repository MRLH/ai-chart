import ResponseCode from '@/constants/ResponseCode'
import $ajax from '@/utils/uAjax'
import router from '../router'

const ctx = ''

/* opt: {失败弹框} */
function handleResponse (response, success, failed, opt) {
  const vm = router.app
  const data = response.data
  if (data.code === ResponseCode.SUCCESS) {
    success(data)
    if (opt.successW) {
    }
  } else if (data.code === ResponseCode.UNLOGIN) {
    // 如果不是登录页面则跳转登录页面
    if (vm.$route.path !== '/login') {
      vm.$router.push({
        path: '/login',
        query: {
          redirect: vm.$route.fullPath
        }
      })
    }
  } else if (data.code === ResponseCode.WARNING) {
    console.warn(data.msg)
    failed(data)
  } else {
    if (opt.failedW) {
    }
    failed(data)
  }
}

export default {
  // 获取带上下文的url地址
  getUrl: function (url) {
    return ctx + url
  },
  /**
   * get默认成功无弹窗，失败有弹窗
   * successW 成功是否弹窗
   * failedW 失败是否弹窗
   * extra 额外的参数
   */
  makeGet: function (url, options) {
    options = options || {}
    const _options = {extra: {}, successW: false, failedW: true}
    Object.assign(_options, options)
    return function (params) {
      params = params || {}
      return new Promise(function (resolve, reject) {
        params._r = Date.now()
        $ajax.get(ctx + url, {
          params: Object.assign({
            _r: Date.now()
          }, _options.extra, params)
        }).then(function (response) {
          handleResponse(response, resolve, reject, _options)
        }).catch(function (e) {
          reject({
            msg: '网络异常'
          })
        })
      })
    }
  },
  /**
   * post默认成功失败都有弹窗
   * successW 成功是否弹窗
   * failedW 失败是否弹窗
   * extra 额外的参数
   */
  makePost: function (url, options) {
    options = options || {}
    const _options = {extra: {}, successW: true, failedW: true}
    Object.assign(_options, options)
    return function (params) {
      params = params || {}
      return new Promise(function (resolve, reject) {
        $ajax.fPost(ctx + url, Object.assign({}, _options.extra, params)).then(function (response) {
          handleResponse(response, resolve, reject, _options)
        }).catch(function (e) {
          reject({
            msg: '网络异常'
          })
        })
      })
    }
  },
  /**
   * post Json格式的数据
   * post默认成功失败都有弹窗
   * successW 成功是否弹窗
   * failedW 失败是否弹窗
   * extra 额外的参数
   */
  makeJPost: function (url, options) {
    options = options || {}
    const _options = {extra: {}, successW: true, failedW: true}
    Object.assign(_options, options)
    return function (params) {
      params = params || {}
      return new Promise(function (resolve, reject) {
        $ajax.jPost(ctx + url, Object.assign({}, _options.extra, params)).then(function (response) {
          handleResponse(response, resolve, reject, _options)
        }).catch(function (e) {
          reject({
            msg: '网络异常'
          })
        })
      })
    }
  },
  /**
   * get默认成功无弹窗，失败有弹窗, 支持pathVariable 例： /api/order/detail/{orderId}
   * successW 成功是否弹窗
   * failedW 失败是否弹窗
   * extra 额外的参数
   */
  restGet (url, options = {}) {
    let _options = {extra: {}, successW: false, failedW: true, canCancel: true, needRedirect: false}
    Object.assign(_options, options)
    return function (id) {
      return new Promise((resolve, reject) => {
        $ajax.get(ctx + url + id, {
          params: {
            _r: new Date().getTime(),
            ..._options.extra
          }
        }).then(response => {
          handleResponse(response, resolve, reject, _options)
        }).catch(e => {
          reject({
            msg: '网络异常'
          })
        })
      })
    }
  }
}
