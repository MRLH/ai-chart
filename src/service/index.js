import chart from './src/chartService'
// 在这个里面注册服务
const Service = {
  chart
}

// install方法不做任何修改
function install (Clazz) {
  Clazz.prototype.$service = Service
}

/*
 * 使用方法
 * this.$service.xxx.function()
 */

export default {
  install
}
