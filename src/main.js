import Vue from 'vue'
import App from './App'
import router from './router'
import service from './service'
import components from './components'
import 'es6-promise/auto'

Vue.use(service)
Vue.use(components)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
