import Axios from 'axios'
import qs from 'qs'

// 在这个文件可以对axios进行一些初始化配置

const ajax = Axios
ajax.fPost = (url, param, config = null) => {
  return Axios.post(url, qs.stringify(param, {indices: false}), config)
}

ajax.jPost = (url, param, config = {headers: {'Content-Type': 'application/json'}}) => {
  return Axios.post(url, JSON.stringify(param), config)
}

export default ajax
